﻿/**
 * 红黑二叉树头文件
 */

#ifndef INCLUDE_YH_RB_TREE_H_
#define INCLUDE_YH_RB_TREE_H_

#include "yh_define.h"

/**
 * 红黑树节点结构体
 */
typedef struct yh_rbtree_node_s yh_rbtree_node_t;
struct yh_rbtree_node_s {

	// 键值(在整个树中唯一)
	int key;

	// 左右子节点,默认为哨兵节点
	yh_rbtree_node_t *left;
	yh_rbtree_node_t *right;

	// 父节点,默认为NULL
	yh_rbtree_node_t *parent;

	// 节点颜色, 1 表示红色, 0 表示黑色
	yh_byte color;

	// 节点数据
	void *data;
};

/**
 * 红黑树结构体
 */
typedef struct {

	// 根节点,默认为哨兵节点
	yh_rbtree_node_t *root;

	// 哨兵节点
	yh_rbtree_node_t s;
	yh_rbtree_node_t *sentinel;

} yh_rbtree_t;

// 初始化节点与二叉树
void yh_rbtree_init(yh_rbtree_t *rbtree);
void yh_rbtree_node_init(yh_rbtree_t *rbtree, yh_rbtree_node_t *node);

// 查找节点
yh_rbtree_node_t* yh_rbtree_find(yh_rbtree_t *rbtree, int key);

// 插入节点
int yh_rbtree_insert(yh_rbtree_t *rbtree, yh_rbtree_node_t *node);

// 删除节点
yh_rbtree_node_t* yh_rbtree_delete(yh_rbtree_t *rbtree, int key);
yh_rbtree_node_t* yh_rbtree_delete(yh_rbtree_t *rbtree, yh_rbtree_node_t *node);

#endif // INCLUDE_YH_RB_TREE_H_