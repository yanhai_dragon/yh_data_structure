﻿/**
 * 公共标准定义
 */

#ifndef INCLUDE_YH_DEFINE_H_
#define INCLUDE_YH_DEFINE_H_

#include <stdio.h>
#include <stdbool.h>

enum yh_ret
{
	YH_OK = 0,
	YH_ERROR = -1,
	YH_AGAIN = -2,
	YH_BUSY = -3,
	YH_DONE = -4,
	YH_DECLINED = -5,
	YH_ABORT = -6,
};

#define IS_NULL(p) (NULL == p)

#define yh_byte unsigned char

#endif // INCLUDE_YH_DEFINE_H_
