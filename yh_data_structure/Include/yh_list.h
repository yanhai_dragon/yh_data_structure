﻿/**
 * 双向链表头文件，并且不再实现单链表
 */

#ifndef INCLUDE_YH_LIST_H_
#define INCLUDE_YH_LIST_H_

#include "yh_define.h"

/**
 * 链表节点结构体
 */
typedef struct yh_list_node_s yh_list_node_t;
struct yh_list_node_s
{
	// 双向链表指针
	yh_list_node_t *prev;
	yh_list_node_t *next;

	// 链表节点数据
	void *data;
};

/**
 * 链表结构体
 */
typedef struct
{
	// 保存链表头和链表尾的地址,
	// 也就是哨兵节点的地址
	yh_list_node_t *first;
	yh_list_node_t *last;

	// 哨兵节点
	yh_list_node_t sentinel_head;
	yh_list_node_t sentinel_tail;

	// 链表中节点的个数,不包括哨兵节点
	int size;

} yh_list_t;

// 初始化节点与链表
void yh_list_node_init(yh_list_node_t *node);
void yh_list_init(yh_list_t *list);

// 向链表中添加节点
int yh_list_push_back(yh_list_t *list, yh_list_node_t *node);
int yh_list_push_front(yh_list_t *list, yh_list_node_t *node);
int yh_list_insert(yh_list_t *list, int n, yh_list_node_t *node);

// 在链表中查询节点
yh_list_node_t* yh_list_find(yh_list_t *list, int n);
int yh_list_find(yh_list_t *list, yh_list_node_t *node);

// 在链表中移除节点
yh_list_node_t* yh_list_pop_back(yh_list_t *list);
yh_list_node_t* yh_list_pop_front(yh_list_t *list);
yh_list_node_t* yh_list_remove(yh_list_t *list, int n);
int yh_list_remove(yh_list_t *list, yh_list_node_t *node);

// 遍历链表
/* 返回 0 表示继续遍历,返回其他表示终止遍历 */
typedef int(*yh_list_foreach_pt)(yh_list_node_t*, void*);
/* direction 为 0 表示从前向后遍历,其他表示从后向前遍历 */
void yh_list_foreach(yh_list_t *list, yh_list_foreach_pt func_pt,
	void *context = NULL, int direction = 0);

// 清理链表,释放空间
typedef void(*yh_list_node_clear_pt)(yh_list_node_t*);
void yh_list_clear(yh_list_t *list, yh_list_node_clear_pt func_pt = NULL);

#endif // INCLUDE_YH_LIST_H_
