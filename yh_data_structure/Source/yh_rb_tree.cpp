﻿#include "../Include/yh_rb_tree.h"

#define SET_RED(node)			(node->color = 1)
#define SET_BLACK(node)			(node->color = 0)
#define IS_RED(node)			(node->color)
#define IS_BLACK(node)			(!node->color)

#define IS_LEFT(node)			(IS_NULL(node->parent) ? false : ((node->parent->left == node) ? true : false))
#define IS_RIGHT(node)			(IS_NULL(node->parent) ? false : ((node->parent->right == node) ? true : false))
#define IS_LEAF(node)			((node->left != &sentinel) ? false :((node->right != &sentinel) ? false : true))

// 全局哨兵节点,所有的红黑树使用该哨兵节点
static yh_rbtree_node_t sentinel = { 0,NULL,NULL,NULL,0,NULL };

static void yh_rbtree_left_rotation(yh_rbtree_node_t *node)
{
	yh_rbtree_node_t *a = node;
	yh_rbtree_node_t *b = a->right;// 假设一定存在
	yh_rbtree_node_t *p = a->parent;
	yh_rbtree_node_t *y = b->left;
	
	a->right = y;
	if (y != &sentinel)
	{
		y->parent = a;
	}

	b->left = a;
	a->parent = b;

	b->parent = p;
	if (!IS_NULL(p))
	{
		if (p->left == a)
		{
			p->left = b;
		}
		else
		{
			p->right = b;
		}
	}
}

static void yh_rbtree_right_rotation(yh_rbtree_node_t *node)
{
	yh_rbtree_node_t *a = node;
	yh_rbtree_node_t *b = a->left;// 假设一定存在
	yh_rbtree_node_t *p = a->parent;
	yh_rbtree_node_t *y = b->left;

	a->left = y;
	if (y != &sentinel)
	{
		y->parent = a;
	}

	b->right = a;
	a->parent = b;

	b->parent = p;
	if (!IS_NULL(p))
	{
		if (p->left == a)
		{
			p->left = b;
		}
		else
		{
			p->right = b;
		}
	}
}

static bool yh_rbtree_node_insert(yh_rbtree_t *rbtree, yh_rbtree_node_t *child)
{
	int num = 0;
	yh_rbtree_node_t *p = rbtree->root;

	while (true)
	{
		num = p->key - child->key;
		if ((num > 0) && (p->left != rbtree->sentinel))
		{
			p = p->left;
		}
		else if ((num < 0) && (p->right != rbtree->sentinel))
		{
			p = p->right;
		}
		else
		{
			if (num < 0)
			{
				p->right = child;
			}
			else if (num > 0)
			{
				p->left = child;
			}
			else
			{
				return false;
			}

			child->parent = p;
			return true;
		}
	}
}

void yh_rbtree_init(yh_rbtree_t *rbtree)
{
	if (IS_NULL(rbtree))
	{
		return;
	}

	rbtree->s = sentinel;
	rbtree->sentinel = &rbtree->s;
	rbtree->root = rbtree->sentinel;
}

void yh_rbtree_node_init(yh_rbtree_t *rbtree, yh_rbtree_node_t *node)
{
	if (IS_NULL(rbtree) || IS_NULL(node))
	{
		return;
	}

	// 新的节点必须设置为红色
	SET_RED(node);

	// 设置哨兵节点
	node->left = rbtree->sentinel;
	node->right = rbtree->sentinel;

	node->parent = NULL;
	node->data = NULL;
	node->key = 0;
}

yh_rbtree_node_t* yh_rbtree_find(yh_rbtree_t *rbtree, int key)
{
	if (IS_NULL(rbtree))
	{
		return NULL;
	}
	yh_rbtree_node_t *node = rbtree->root;

	while (node != rbtree->sentinel)
	{
		if (node->key == key)
		{
			break;
		}
		else if (node->key > key)
		{
			node = node->left;
		}
		else
		{
			node = node->right;
		}
	}

	return (node == rbtree->sentinel) ? NULL : node;
}

int yh_rbtree_insert(yh_rbtree_t *rbtree, yh_rbtree_node_t *node)
{
	if (IS_NULL(rbtree) || IS_NULL(node))
	{
		return YH_ERROR;
	}

	if (rbtree->root == rbtree->sentinel)
	{
		SET_BLACK(node);
		rbtree->root = node;
		return YH_OK;
	}

	// 插入新节点
	if (!yh_rbtree_node_insert(rbtree, node))
	{
		return YH_ERROR;
	}

	// 关注节点如果是根节点,或者关注节点的父节点是黑色,就不用调整了
	while ((rbtree->root != node) && IS_RED(node->parent))
	{
		if (IS_LEFT(node->parent))
		{
			if (IS_RED(node->parent->parent->right))
			{
				SET_BLACK(node->parent);
				SET_BLACK(node->parent->parent->right);
				SET_RED(node->parent->parent);

				// 关注节点变为关注节点的爷爷节点
				node = node->parent->parent;
			}
			else
			{
				if (IS_RIGHT(node))
				{
					yh_rbtree_left_rotation(node->parent);
					// 关注节点变为关注节点原来的父节点
					node = node->left;
				}

				SET_BLACK(node->parent);
				SET_RED(node->parent->parent);
				yh_rbtree_right_rotation(node->parent->parent);
			}
		}
		else
		{
			if (IS_RED(node->parent->parent->left))
			{
				SET_BLACK(node->parent);
				SET_BLACK(node->parent->parent->left);
				SET_RED(node->parent->parent);

				// 关注节点变为关注节点的爷爷节点
				node = node->parent->parent;
			}
			else
			{
				if (IS_LEFT(node))
				{
					yh_rbtree_right_rotation(node->parent);
					// 关注节点变为关注节点原来的父节点
					node = node->right;
				}

				SET_BLACK(node->parent);
				SET_RED(node->parent->parent);
				yh_rbtree_left_rotation(node->parent->parent);
			}
		}
	}

	// 在前面调整的时候,根节点的颜色可能会被设置成了红色
	SET_BLACK(rbtree->root);
	return YH_OK;
}

yh_rbtree_node_t* yh_rbtree_delete(yh_rbtree_t *rbtree, int key)
{
	return yh_rbtree_delete(rbtree, yh_rbtree_find(rbtree, key));
}

/**
 *实现原理,参考链接"https://blog.csdn.net/dearQiHao/article/details/102886249"
 */
yh_rbtree_node_t* yh_rbtree_delete(yh_rbtree_t *rbtree, yh_rbtree_node_t *node)
{
	if (IS_NULL(rbtree) || IS_NULL(node))
	{
		return NULL;
	}

	// 继任者
	yh_rbtree_node_t *successor = NULL;
	// successor的兄弟节点
	yh_rbtree_node_t *brother = NULL;
	// 被删除者
	yh_rbtree_node_t *remove = NULL;
	// remove的颜色
	bool red;

	// 确认要被删除的节点和继承节点
	if (node->left == rbtree->sentinel)
	{
		successor = node->right;
		remove = node;
	}
	else if (node->right == rbtree->sentinel)
	{
		successor = node->left;
		remove = node;
	}
	else
	{
		remove = node->right;
		while (remove->left != rbtree->sentinel)
		{
			remove = remove->left;
		}
		successor = remove->right;
	}

	// 如果要被删除的节点是root节点
	if (remove == rbtree->root)
	{
		// successor成为新的roo节点
		successor->parent = NULL;
		SET_BLACK(successor);
		rbtree->root = successor;

		goto END;
	}

	// 第一次替换,用successor取代remove
	if (IS_LEFT(remove))
	{
		remove->parent->left = successor;
	}
	else
	{
		remove->parent->right = successor;
	}

	red = IS_RED(remove);

	if (remove == node)
	{
		successor->parent = remove->parent;
	}
	else
	{
		if (remove->parent == node)
		{
			successor->parent = remove;
		}
		else
		{
			successor->parent = remove->parent;
		}

		// 第二次替换,用remove替换node
		remove->left = node->left;
		remove->right = node->right;
		remove->parent = node->parent;
		remove->color = node->color;

		if (node = rbtree->root)
		{
			// remove成为新的roo节点
			remove->parent = NULL;
			rbtree->root = remove;
		}
		else
		{
			if (IS_LEFT(node))
			{
				node->parent->left = remove;
			}
			else
			{
				node->parent->right = remove;
			}
		}

		if (remove->left != rbtree->sentinel)
		{
			remove->left->parent = remove;
		}
		if (remove->right != rbtree->sentinel)
		{
			remove->right->parent = remove;
		}
	}

	if (red)
	{
		goto END;
	}

	while ((successor != rbtree->root) && (IS_BLACK(successor)))
	{
		if (IS_LEFT(successor))
		{
			brother = successor->parent->right;

			if (IS_RED(brother))
			{
				SET_BLACK(brother);
				SET_RED(successor->parent);
				yh_rbtree_left_rotation(successor->parent);
				brother = successor->parent->right;
			}

			if (IS_BLACK(brother->left) && IS_BLACK(brother->right))
			{
				SET_RED(brother);
				successor = successor->parent;
			}
			else
			{
				if (IS_BLACK(brother->right))
				{
					SET_BLACK(brother->left);
					SET_RED(brother);
					yh_rbtree_right_rotation(brother);
					brother = successor->parent->right;
				}

				brother->color = successor->parent->color;
				SET_BLACK(successor->parent);
				SET_BLACK(brother->right);
				yh_rbtree_left_rotation(successor->parent);
				successor = rbtree->root;
			}
		}
		else
		{
			brother = successor->parent->left;

			if (IS_RED(brother))
			{
				SET_BLACK(brother);
				SET_RED(successor->parent);
				yh_rbtree_right_rotation(successor->parent);
				brother = successor->parent->left;
			}

			if (IS_BLACK(brother->left) && IS_BLACK(brother->right))
			{
				SET_RED(brother);
				successor = successor->parent;
			}
			else
			{
				if (IS_BLACK(brother->left))
				{
					SET_BLACK(brother->right);
					SET_RED(brother);
					yh_rbtree_left_rotation(brother);
					brother = successor->parent->left;
				}

				brother->color = successor->parent->color;
				SET_BLACK(successor->parent);
				SET_BLACK(brother->left);
				yh_rbtree_right_rotation(successor->parent);
				successor = rbtree->root;
			}
		}
	}

	SET_BLACK(successor);

END:
	node->left = &sentinel;
	node->right = &sentinel;
	node->parent = NULL;
	SET_RED(node);
	return node;
}
