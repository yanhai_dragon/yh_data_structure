﻿#include "../Include/yh_list.h"
#include <corecrt_malloc.h>

static void node_insert(yh_list_node_t *prev,
	yh_list_node_t *next, yh_list_node_t *node)
{
	node->next = next;
	node->prev = prev;
	prev->next = node;
	next->prev = node;
}

static void node_remove(yh_list_node_t *node)
{
	yh_list_node_t *prev = node->prev;
	yh_list_node_t *next = node->next;

	prev->next = next;
	next->prev = prev;
}

static void yh_list_node_clear(yh_list_node_t *node)
{
	free(node->data);
	free((void*)node);
}

void yh_list_node_init(yh_list_node_t *node)
{
	if (IS_NULL(node))
	{
		return;
	}

	node->prev = NULL;
	node->next = NULL;
	node->data = NULL;
}

void yh_list_init(yh_list_t *list)
{
	if (IS_NULL(list))
	{
		return;
	}

	list->first = &list->sentinel_head;
	list->last = &list->sentinel_tail;

	yh_list_node_init(list->first);
	yh_list_node_init(list->last);

	list->first->next = list->last;
	list->last->prev = list->first;

	list->size = 0;
}

int yh_list_push_back(yh_list_t *list, yh_list_node_t *node)
{
	if (IS_NULL(list) || IS_NULL(node))
	{
		return YH_ERROR;
	}

	yh_list_node_t *prev = list->last->prev;
	yh_list_node_t *next = list->last;

	node_insert(prev, next, node);
	++list->size;

	return YH_OK;
}

int yh_list_push_front(yh_list_t *list, yh_list_node_t *node)
{
	if (IS_NULL(list) || IS_NULL(node))
	{
		return YH_ERROR;
	}

	yh_list_node_t *prev = list->first;
	yh_list_node_t *next = list->first->next;

	node_insert(prev, next, node);
	++list->size;

	return YH_OK;
}

int yh_list_insert(yh_list_t *list, int n, yh_list_node_t *node)
{
	if (IS_NULL(list) || IS_NULL(node))
	{
		return YH_ERROR;
	}

	yh_list_node_t *next = yh_list_find(list, n);
	if (!next)
	{
		return YH_ERROR;
	}
	yh_list_node_t *prev = next->prev;

	node_insert(prev, next, node);
	++list->size;

	return YH_OK;
}

yh_list_node_t* yh_list_find(yh_list_t *list, int n)
{
	++n;

	if (IS_NULL(list) || (list->size < 1)
		|| (n < 1) || (list->size < n))
	{
		return NULL;
	}

	yh_list_node_t *node = NULL;
	if (n < (list->size / 2))
	{
		int i = 1;
		node = list->first->next;
		while (i < n)
		{
			++i;
			node = node->next;
		}
	}
	else
	{
		int i = list->size;
		node = list->last->prev;
		while (i > n)
		{
			--i;
			node = node->prev;
		}
	}

	return node;
}

int yh_list_find(yh_list_t *list, yh_list_node_t *node)
{
	if (IS_NULL(list) || IS_NULL(node) || (list->size < 1))
	{
		return YH_ERROR;
	}

	int index = 1;
	yh_list_node_t *next = list->first->next;

	list->last->next = node;
	while (next != node)
	{
		++index;
		next = next->next;
	}
	list->last->next = NULL;

	if (index > list->size)
	{
		return YH_ERROR;
	}
	else
	{
		return index - 1;
	}
}

yh_list_node_t* yh_list_pop_back(yh_list_t *list)
{
	if (IS_NULL(list) || (list->size < 1))
	{
		return NULL;
	}

	yh_list_node_t *node = list->last->prev;

	node_remove(node);
	--list->size;

	return node;
}

yh_list_node_t* yh_list_pop_front(yh_list_t *list)
{
	if (IS_NULL(list) || (list->size < 1))
	{
		return NULL;
	}

	yh_list_node_t *node = list->first->next;

	node_remove(node);
	--list->size;

	return node;
}

yh_list_node_t* yh_list_remove(yh_list_t *list, int n)
{
	if (IS_NULL(list))
	{
		return NULL;
	}

	yh_list_node_t *node = yh_list_find(list, n);
	if (!node)
	{
		return NULL;
	}

	node_remove(node);
	--list->size;

	return node;
}

int yh_list_remove(yh_list_t *list, yh_list_node_t *node)
{
	if (IS_NULL(list) || IS_NULL(node))
	{
		return YH_ERROR;
	}

	int index = yh_list_find(list, node);
	if (YH_ERROR == index)
	{
		return YH_ERROR;
	}

	node_remove(node);
	--list->size;

	return index;
}

void yh_list_foreach(yh_list_t *list, yh_list_foreach_pt func_pt,
	void *context /*= NULL*/, int direction /*= 0*/)
{
	if (IS_NULL(list) || IS_NULL(func_pt))
	{
		return;
	}

	yh_list_node_t *node = NULL;
	if (!direction)
	{
		node = list->first->next;
		while (node != list->last)
		{
			if (func_pt(node, context))
			{
				break;
			}
			node = node->next;
		}
	}
	else
	{
		node = list->last->prev;
		while (node != list->first)
		{
			if (func_pt(node, context))
			{
				break;
			}
			node = node->prev;
		}
	}
}

void yh_list_clear(yh_list_t *list, yh_list_node_clear_pt func_pt /*= NULL*/)
{
	if (IS_NULL(list))
	{
		return;
	}
	if (IS_NULL(func_pt))
	{
		func_pt = yh_list_node_clear;
	}

	yh_list_node_t *next = NULL;
	yh_list_node_t *node = list->first->next;
	while (node != list->last)
	{
		next = node->next;
		func_pt(node);
		node = next;
	}

	yh_list_init(list);
}
